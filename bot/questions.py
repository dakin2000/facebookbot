financial_freedom_question = {
						    "text":"Do you want me to calculate your financial freedom for you?",
						    "quick_replies":[
						      {
						        "content_type":"text",
						        "title":"Yes",
						        "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_YES"
						      },
						      {
						        "content_type":"text",
						        "title":"No",
						        "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_NO"
						      }
						    ]
						  }
service_options = {
            "text":"I Can help you with the following.",
            "quick_replies":[
              {
                "content_type":"text",
                "title":"Personal Finance",
                "payload":"P"
              },
              {
                "content_type":"text",
                "title":"Financial Freedom",
                "payload":"F"
              }
            ]
          }

salary_question = {
            "text":"How much is your current salary.",
            "quick_replies":[
                {
                  "content_type":"text",
                  "title":"salary_answer",
                  "payload":"salary"
                }
              ]
          }

get_started = [
          {
            "type":"web_url",
            "url":"https://cowrywise.com/how/",
            "title":"How it works"
          },
          {
            "type":"postback",
            "title":"Sign Up",
            "payload":"USER_DEFINED_PAYLOAD_SIGNUP"
          }
        ]

get_started_registered = {
            "text":"Get started with any of these actions.",
            "quick_replies":[
              {
                "content_type":"text",
                "title":"Save",
                "payload":"save"
              },
              {
                "content_type":"text",
                "title":"Withdraw",
                "payload":"withdraw"
              },
              {
                "content_type":"text",
                "title":"Detail",
                "payload":"check account detail"
              }
            ]
          }