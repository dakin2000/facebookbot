from numpy import nper, pv, fv


def cal_financial_freedom(annual_returns,monthly_income,savings_rate,current_savings):
	number_of_years = nper(annual_returns/12, -monthly_income*savings_rate,-current_savings, (monthly_income*(1-savings_rate)*12)/0.04) / 12 
	present_value = pv(annual_returns/12, number_of_years*12, 0.0)
	future_value = -fv(annual_returns/12, number_of_years*12, monthly_income*savings_rate, -present_value)
	return future_value


# annual_returns #annual interest rate you get on your savings
# monthly_income #monthly salary
# savings_rate #% of income saved
# current_savings #current savings

# example
# finance = cal_financial_freedom(0.30,500000,0.80,350000)