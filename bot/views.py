# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from pprint import pprint
from pymessenger.bot import Bot
import facebook
import requests
import sys

from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
from enum import Enum
from .models import UserFinancialSession
from .questions import *
from .session_mem import glob_mem

from django.views import generic
from django.http.response import HttpResponse

from .utils import cal_financial_freedom
# Create your views here.
ACCESS_TOKEN = 'EAAZAZBLCyEwOkBAJt0rmVpgYIUuATc2mBWNT4gxUoVkUgEHerZAM91R1JFKngqdkK9ZBqZB41FtXTbAdqDuqyrFYZAN4uQUtZCBWjdAgtsAwCaaYYf5YOyCWh2zF31wx11jcRpubSEVPQB5gezsUl6niaWNsQ0Mqrk2E5mvzGftpwZDZD'
bot = Bot(ACCESS_TOKEN)
graph = facebook.GraphAPI(access_token=ACCESS_TOKEN)

class NotificationType(Enum):
    regular = "REGULAR"
    silent_push = "SILENT_PUSH"
    no_push = "NO_PUSH"

class CWBot(generic.View):
    def get(self, request, *args, **kwargs):
        if self.request.GET['hub.verify_token'] == '123456':
            return HttpResponse(self.request.GET['hub.challenge'])
        else:
            return HttpResponse('Error, invalid token')

    # The get method is the same as before.. omitted here for brevity
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    # Post function to handle Facebook messages
    def post(self, request, *args, **kwargs):
        
        output = json.loads(self.request.body.decode('utf-8'))
        pprint(output)
        # log(output)
        for event in output['entry']:
            messaging = event['messaging']
            for x in messaging:
            	recipient_id = x['sender']['id']
                if x.get('postback'):
                    if x.get('postback')['payload'] == 'first hand shake':
                        user_detail = graph.get_object(id=recipient_id)
                        bot.send_text_message(recipient_id, "Welcome {0} {1}. Nice to meet you, I am CowryWise bot.I`m here to help you interact with your cowrywise account,so you can manage your savings easily".format(user_detail['first_name'],user_detail['last_name']))
                        # bot.send_button_message(recipient_id, 'get started with...', get_started)
                        send_quick_replies(recipient_id, get_started_registered)
                    else:
                    	print 'say'                    
                
                if x.get('message'):
                    message = x['message']['text']
                    sender = x['sender']['id']
                    if x['message'].get('quick_reply'):
                        reply = x['message'].get('quick_reply')['payload']
                        self.request.session['get_started'] = reply
                        if self.request.session['get_started']=='save':
                            pass

                        elif self.request.session['get_started']=='withdraw':
                            pass

                        elif self.request.session['get_started']=='check account detail':
                            pass
                        else:
                            pass
                    elif x['message'].get('text'):
                        try:
                            value = float(message)
                            result = value*12
                            bot.send_text_message(recipient_id, "Thank for consulting me on your Financial Freedom")
                            bot.send_text_message(recipient_id, "Based on your salary.")
                            bot.send_text_message(recipient_id, "Your total savings will be N{0} by the end of this year".format(result))
                        except:
                            user_detail = graph.get_object(id=sender)
                            pprint(user_detail)
                            bot.send_text_message(recipient_id, "Welcome {0} {1}. Nice to meet you, I am CowryWise bot.I`m here to help you interact with your cowrywise account,so you can manage your savings easily".format(user_detail['first_name'],user_detail['last_name']))
                            send_quick_replies(recipient_id, service_options)
                    else:
                        pass
        return HttpResponse('Success')


def log(message):  # simple wrapper for logging to stdout on heroku
    pprint(str(message)) 
    # sys.stdout.flush()

def send_quick_replies(recipient_id, elements, notification_type=NotificationType.regular):
        """Send generic messages to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/generic-template
        Input:
            recipient_id: recipient id to send to
            elements: generic message elements to send
        Output:
            Response from API as <dict>
        """
        payload ={
                "recipient":{
                        "id":recipient_id
                      },
                "message":elements
                }

        return bot.send_raw(payload)



def send_message(recipient_id, message):
    '''Send text messages to the specified recipient.
    https://developers.facebook.com/docs/messenger-platform/send-api-reference/text-message
    Input:
        recipient_id: recipient id to send to
        message: raw message to send
    Output:
        Response from API as <dict>
    '''
    payload = {
        'recipient': {
            'id': recipient_id
        },
        'message': message,
        'payload': 'salary'
    }
    return bot.send_raw(payload)

# from .session_mem import glob_mem
# glob_mem.update({'yearly_return':value) 
# glob_mem[k][ar] = 