# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class UserSession(models.Model):
	user_fb_id = models.CharField(max_length=40,primary_key=True)
	username = models.CharField(max_length=100, default='',blank=True,null=True)
	timestamp = models.DateTimeField(auto_now=True,auto_now_add=False)

	def __str__(self):
		return self.user_fb_id

	def __unicode__(self):
		return self.user_fb_id

class UserFinancialSession(models.Model):
    user = models.ForeignKey(UserSession)
    annual_returns = models.FloatField(default=0.00)
    monthly_income = models.FloatField(default=0.00)
    savings_rate = models.FloatField(default=0.00)
    current_savings = models.FloatField(default=0.00)
    future_value = models.FloatField(default=0.00)

    def __str__(self):
        return self.user

    def __unicode__(self):
        return self.user